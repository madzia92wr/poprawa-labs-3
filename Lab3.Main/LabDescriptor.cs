﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IKoła);
        public static Type I2 = typeof(IOdkurzacz);
        public static Type I3 = typeof(ISteruj);

        public static Type Component = typeof(Roomba);

        public static GetInstance GetInstanceOfI1 = (component) => component;
        public static GetInstance GetInstanceOfI2 = (component) => component;
        public static GetInstance GetInstanceOfI3 = (component) => component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Domieszka);
        public static Type MixinFor = typeof(Roomba);

        #endregion
    }
}
