﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Metody z interfejsu IOdkurzacz:");
            IOdkurzacz odkurzacz = new Roomba();
            odkurzacz.Włącz();
            odkurzacz.Wyłącz();

            Console.WriteLine();
            Console.WriteLine("Metody z interfejsu IKoła:");
            IKoła koła = new Roomba();
            koła.RuszKoła();
            koła.ZatrzymajKoła();

            Console.WriteLine();
            Console.WriteLine("Metody z interfejsu ISteruj:");
            ISteruj steruj = new Roomba();
            steruj.Start();
            steruj.Stop();

            Console.WriteLine();
            Console.WriteLine("Pokazanie wykorzystania wzorca fasada:");
            Roomba roomba = new Roomba();
            roomba.Metody_z_sterowanie();
            Console.WriteLine();

            Console.WriteLine("Pokazanie domieszki:");
            roomba.DomieszkaRoomba();
            
            Console.ReadKey();
        }
    }
}
