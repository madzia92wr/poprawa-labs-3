﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Roomba : IKoła, IOdkurzacz, ISteruj
    {
        ISterowanie st;

        public Roomba() 
        {
            st =new Sterowanie();
        }

        public void Metody_z_sterowanie()
        {
            st.Start1();
            st.Stop2();
        }

        void IKoła.RuszKoła()
        {
            Console.WriteLine("Ruszaj kołami");
        }

        void IKoła.ZatrzymajKoła()
        {
            Console.WriteLine("Zatrzymaj Koła");
        }

        void IOdkurzacz.Włącz()
        {
            Console.WriteLine("Włacz");
        }

        void IOdkurzacz.Wyłącz()
        {
            Console.WriteLine("Wyłącz");
        }

        void ISteruj.Start()
        {
            Console.WriteLine("Start");
        }

        void ISteruj.Stop()
        {
            Console.WriteLine("Stop");
        }
    }
}
